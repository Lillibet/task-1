provider "aws" {
  region     = "us-east-1"
  access_key = "AKIAVUZSWNPZMVTTEGUB"
  secret_key = "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"
}

resource "aws_iam_policy" "my-policy1" {
  name        = "my_policy1"
  path        = "/"
  description = "My test policy"
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "cloudformation:DescribeStacks",
                "cloudformation:ListStackResources",
                "cloudwatch:ListMetrics",
                "cloudwatch:GetMetricData",
                "ec2:DescribeSecurityGroups",
                "ec2:DescribeSubnets",
                "ec2:DescribeVpcs",
                "kms:ListAliases",
                "iam:GetPolicy",
                "iam:GetPolicyVersion",
                "iam:GetRole",
                "iam:GetRolePolicy",
                "iam:ListAttachedRolePolicies",
                "iam:ListRolePolicies",
                "iam:ListRoles",
                "lambda:*",
                "logs:DescribeLogGroups",
                "states:DescribeStateMachine",
                "states:ListStateMachines",
                "tag:GetResources",
                "xray:GetTraceSummaries",
                "xray:BatchGetTraces"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:PassRole",
            "Resource": "*",
            "Condition": {
                "StringEquals": {
                    "iam:PassedToService": "lambda.amazonaws.com"
                }
            }
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:DescribeLogStreams",
                "logs:GetLogEvents",
                "logs:FilterLogEvents"
            ],
            "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*"
        }
    ]
}
     )
}

resource "aws_iam_role" "my_role1" {
  name = "my_role1"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })
}


resource "aws_iam_role_policy_attachment" "my-attach" {
    role = aws_iam_role.my_role1.name
    policy_arn = aws_iam_policy.my-policy1.arn
}

data "archive_file" "code-1" {
  type        = "zip"
  source_file = "code-1.py"
  output_path = "$(local.lambda_zip_location)"
}

##data "archive_file" "code-1" {
# type        = "zip"
  #source_file = "${path.module}/init.tpl"
 # output_path = "${path.module}/files/init.zip"
#}
locals{
    lambda_zip_location = "outputs/code-1.zip"
}

resource "aws_lambda_function" "my_lambda" {
    filename = "$(local.lambda_zip_location)"
    function_name = "hello2"
    role = aws_iam_role.my_role1.arn
    handler = "code-1.hello"
    runtime = "python3.8"
    #source_bash = 
}







